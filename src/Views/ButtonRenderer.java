package Views;
import java.awt.Component;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

//BUTTON RENDERER CLASS
class ButtonRenderer extends JButton implements TableCellRenderer {

    //CONSTRUCTOR
    public ButtonRenderer() {
        //SET BUTTON PROPERTIES
        setOpaque(true);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object obj,
            boolean selected, boolean focused, int row, int col) {
        setText((obj == null) ? "" : "Cerrar");
        return this;
    }

}