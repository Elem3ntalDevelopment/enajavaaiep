/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ena;

import java.util.ArrayList;

/**
 *
 * @author javier
 */
public class Data {

    public static ArrayList<User> users = new ArrayList<User>();
    public static ArrayList<String[]> configurations = new ArrayList<String[]>();
    public static ArrayList<Request> requerimientos = new ArrayList<Request>();
    public static ArrayList<String[]> deparaments = new ArrayList<String[]>();
    public static ArrayList<String[]> responsives = new ArrayList<String[]>();

    public static void saveConfigurations() {
        Persistance.saveConfigurations(Data.configurations);
        Data.configurations = Persistance.loadConfigrations();
    }
    public static void saveRequeriments(){
        Persistance.saveRequeriments(Data.requerimientos);
        Data.requerimientos = Persistance.loadRequeriments();
    }
}
