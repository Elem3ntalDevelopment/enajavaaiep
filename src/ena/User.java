package ena;

/**
 *
 * @author Carlos, Javier, leanette
 * La necesidad del modelo es de mantener en una estructura 
 * ordeanda los elementos de validacion de login/usuario
 */
public class User {

    public int id;
    public String name;
    public String password;

    public User(String name, String password){
        this.name = name;
        this.password = password;
    }
    
    public boolean login(String password){
        return this.password.equals(password) && this.password.length()>1;
    }
}
