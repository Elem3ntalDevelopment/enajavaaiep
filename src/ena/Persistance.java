/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ena;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/**
 *
 * @author javier
 */
public class Persistance {

    public static String pathFolder = "Data/";
    public static String userModel = pathFolder + "user.csv";
    public static String configurations = pathFolder + "configurations.csv";
    public static String requeriments = pathFolder + "requeriments.csv";
    public static String responsives = pathFolder + "responsives.csv";
    public static String deptos = pathFolder + "deptos.csv";

    public static ArrayList<User> loadUsers() {
        ArrayList<User> listUsers = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(Persistance.userModel))) {
            for (String line; (line = br.readLine()) != null;) {
                String[] data_user = line.split(",");
                if (data_user.length == 2) 
                    listUsers.add(new User(data_user[0], data_user[1]));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return listUsers;
    }

    public static ArrayList<String[]> loadConfigrations() {
        ArrayList<String[]> configs = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(Persistance.configurations))) {
            for (String line; (line = br.readLine()) != null;) {
                String[] config_data = line.split(",");
                if (config_data.length == 2)
                    configs.add(config_data);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return configs;
    }

    public static ArrayList<String[]> loadResponsives() {
        ArrayList<String[]> responsives = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(Persistance.responsives))) {
            for (String line; (line = br.readLine()) != null;) {
                String[] responsive_data = line.split(",");
                if (responsive_data.length == 2)
                    responsives.add(responsive_data);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return responsives;
    }

    public static ArrayList<String[]> loadDepartaments() {
        ArrayList<String[]> departaments = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(Persistance.deptos))) {
            for (String line; (line = br.readLine()) != null;) {
                String[] departament_data = line.split(",");
                if (departament_data.length == 2)
                    departaments.add(departament_data);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return departaments;
    }

    static void saveConfigurations(ArrayList<String[]> configurations) {
        try {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(Persistance.configurations))));
            for (String[] line : configurations) {
                StringBuilder csvLine = new StringBuilder();
                for (String data : line) {
                    csvLine.append(data);
                    csvLine.append(",");
                }
                bw.write(csvLine.toString());
                bw.newLine();
            }
            bw.close();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    static ArrayList<Request> loadRequeriments() {
        ArrayList<Request> requeriments = new ArrayList<Request>();
        try (BufferedReader br = new BufferedReader(new FileReader(Persistance.requeriments))) {
            for (String line; (line = br.readLine()) != null;) {
                String[] requeriment_raw = line.split("&");
                if (requeriment_raw.length == 6)
                    requeriments.add(new Request(requeriment_raw[0],
                            requeriment_raw[1],
                            requeriment_raw[2],
                            requeriment_raw[3],
                            requeriment_raw[4],
                            Boolean.parseBoolean(requeriment_raw[5])));
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        return requeriments;
    }

    public static void saveRequeriments(ArrayList<Request> requeriments) {
        try {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(Persistance.requeriments))));
            for (Request requeriment : requeriments) {
                StringBuilder csvLine = new StringBuilder();
                csvLine.append(requeriment.gerencia);
                csvLine.append("&");
                csvLine.append(requeriment.departamentoOrigen);
                csvLine.append("&");
                csvLine.append(requeriment.departamentoResponsable);
                csvLine.append("&");
                csvLine.append(requeriment.encargado);
                csvLine.append("&");
                csvLine.append(requeriment.requeriment);
                csvLine.append("&");
                csvLine.append(requeriment.status);
                bw.write(csvLine.toString());
                bw.newLine();
            }
            bw.close();

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
