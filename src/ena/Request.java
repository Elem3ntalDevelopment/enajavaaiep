package ena;

/**
 * @author Carlos, Javier, Leanette
 * La necesidad del modelo request es para poder controlar los elementos 
 * de requerimentos en cuanto a su estado de resolucion
 */
public class Request {
    public String gerencia;
    public String departamentoOrigen;
    public String departamentoResponsable;
    public String encargado;
    public String requeriment;
    public boolean status;
    public Request(String gerencia, 
                         String departamentoOrigen,
                         String departamentoResponsable,
                         String encargado,
                         String requeriment,
                         boolean status){
        this.gerencia = gerencia;
        this.departamentoOrigen = departamentoOrigen;
        this.departamentoResponsable = departamentoResponsable;
        this.encargado = encargado;
        this.requeriment = requeriment;
        this.status = status;
    }
}
